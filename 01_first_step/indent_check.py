# -*- coding: utf-8 -*-

print u'モジュールのロード'

# モジュール定義
def test():
    print 'test module called.'
    print __name__

# ifの処理
if __name__ == '__main__':
    print u'true:mainとして呼び出された'
    test()
